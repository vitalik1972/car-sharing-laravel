import Vue from 'vue'
import VueRouter from 'vue-router'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import App from './components/App/App'
import Home from './components/Home/Home'
import VendorIndex from './components/vendor/index'
import VendorCreate from './components/vendor/create/create'
import VendorUpdate from './components/vendor/update/update'

import MarkIndex from './components/mark/index/index'
import MarkCreate from './components/mark/create/create'
import MarkUpdate from './components/mark/update/update'

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/vendor',
            name: 'vendorIndex',
            component: VendorIndex
        },
        {
            path: '/vendor/create',
            name: 'vendorCreate',
            component: VendorCreate
        },
        {
            path: '/vendor/update/:id',
            name: 'vendorUpdate',
            component: VendorUpdate
        },

        {
            path: '/mark',
            name: 'markIndex',
            component: MarkIndex
        },
        {
            path: '/mark/create',
            name: 'markCreate',
            component: MarkCreate
        },
        {
            path: '/mark/update/:id',
            name: 'markUpdate',
            component: MarkUpdate
        }
    ],
});

const app = new Vue({
    el: '#app',
    components: {App},
    router,
});
