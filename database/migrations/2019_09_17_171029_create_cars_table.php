<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('vendor_id');
            $table->bigInteger('mark_id');
            $table->string('title')->comment('Наименование');
            $table->string('vin')->comment('VIN');
            $table->string('number',8);
            $table->float('lon')->comment('Долгота');
            $table->float('lat')->comment('Широта');
            $table->bigInteger('renter_id')->comment('Последний арендатор');
            $table->float('total_time_in_use')->comment('Общее время использования');
            $table->tinyInteger('status')->comment('Статус');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
