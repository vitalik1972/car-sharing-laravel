<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('car_id');
            $table->bigInteger('renter_id');
            $table->integer('start_time');
            $table->integer('end_time');
            $table->integer('total_time');
            $table->tinyInteger('status');
            $table->text('description');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_logs');
    }
}
