<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'cars',
            'id' => $this->id,
            'attributes' => [
                'title' => $this->title,
                'vin' => $this->vin,
                'number' => $this->number,
                'lon' => $this->lon,
                'lat' => $this->lat,
                'total_time_in_use' => $this->total_time_in_use,
                'status' => $this->status,
            ],
            'relationship' => new CarRelationshipResource($this),
            'links' => [
                'self' => route('cars.show', ['car' => $this->id]),
            ],
        ];
    }
}
