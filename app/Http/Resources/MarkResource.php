<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MarkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'marks',
            'id' => $this->id,
//            'attributes' => [
                'title' => $this->title,
//            ],
            'relationships' => new MarkRelationshipResource($this),
            'links'         => [
                'self' => route('marks.show', ['mark' => $this->id]),
            ],
        ];
    }
}
