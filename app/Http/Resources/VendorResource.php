<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VendorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'vendors',
            'id' => $this->id,
//            'attributes' => [
                'title' => $this->title,
//            ],
            'relationships' => new VendorRelationshipResource($this),
            'links'         => [
                'self' => route('vendors.show', ['mark' => $this->id]),
            ],
        ];
    }
}
