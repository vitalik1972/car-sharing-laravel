<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RenterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'renters',
            'id' => $this->id,
            'attributes' => [
                'fio' => $this->fio,
                'passport' => $this->passport,
                'address' => $this->addres,
                'phone' => $this->phone,
                'email' => $this->email,
                'driveTotal' => $this->drive_total,
                'timeTotal' => $this->time_total,
                'description' => $this->description,
                'status' => $this->status,
            ],
            'relationships' => new RenterRelationshipResource($this),
            'links' => [
                'self' => route('renters.show', ['renter' => $this->id]),
            ]
        ];
    }
}
