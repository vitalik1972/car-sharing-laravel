<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VendorRequest;
use App\Http\Resources\VendorResource;
use App\Http\Resources\VendorsResource;
use App\Vendor;
use Illuminate\Http\Request;

class VendorController extends Controller
{

    public function update(VendorRequest $request, int $id)
    {
        $vendor = Vendor::findOrFail($id);
        $vendor->update($request->all());
        return $vendor;
    }
    public function destroy(int $id)
    {
        $vendor = Vendor::findOrFail($id);
        if ($vendor->delete())
            return response(null,204);
    }

    public function store(VendorRequest $request)
    {
        $vendor = Vendor::create($request->validated());
        return $vendor;
    }

    public function index()
    {
        return new VendorsResource(Vendor::with('marks')->paginate());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        VendorResource::withoutWrapping();
        return new VendorResource($vendor);
    }

}
