<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Mark
 *
 * @property int $id
 * @property int $vendor_id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Car[] $cars
 * @property-read int|null $cars_count
 * @property-read \App\Vendor $vendor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mark newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mark newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mark query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mark whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mark whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mark whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mark whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mark whereVendorId($value)
 * @mixin \Eloquent
 */
class Mark extends Model
{
    protected $fillable = ['vendor_id', 'title'];

    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }

    public function cars()
    {
        return $this->hasMany('App\Car');
    }
}
