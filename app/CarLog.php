<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CarLog
 *
 * @property int $id
 * @property int $car_id
 * @property int $renter_id
 * @property int $start_time
 * @property int $end_time
 * @property int $total_time
 * @property int $status
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Car $car
 * @property-read \App\Renter $renter
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog whereRenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog whereTotalTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarLog extends Model
{

    public function car()
    {
        return $this->belongsTo('App\Car');
    }

    public function renter()
    {
        return $this->belongsTo('App\Renter');
    }
}
