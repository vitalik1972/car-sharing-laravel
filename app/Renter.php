<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Renter
 *
 * @property int $id
 * @property int $car_id
 * @property string $fio
 * @property string $passport
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property int $drive_total
 * @property int $time_total
 * @property string|null $description
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Car $car
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CarLog[] $logs
 * @property-read int|null $logs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereDriveTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereFio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter wherePassport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereTimeTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Renter whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Renter extends Model
{
    public function logs()
    {
        return $this->hasMany('App\CarLog');
    }

    public function car()
    {
        return $this->belongsTo('App\Car');
    }
}
