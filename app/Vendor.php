<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Vendor
 *
 * @property int $id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Mark[] $marks
 * @property-read int|null $marks_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vendor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vendor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vendor query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vendor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vendor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vendor whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vendor whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Vendor extends Model
{
    protected $fillable = ['title'];

    protected $hidden = ['created_at', 'updated_at'];

    public function marks()
    {
        return $this->hasMany('App\Mark');
    }
}
