<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Car
 *
 * @property int $id
 * @property int $vendor_id
 * @property int $mark_id
 * @property string $title Наименование
 * @property string $vin VIN
 * @property string $number
 * @property float $lon Долгота
 * @property float $lat Широта
 * @property int $renter_id Последний арендатор
 * @property float $total_time_in_use Общее время использования
 * @property int $status Статус
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CarLog[] $logs
 * @property-read int|null $logs_count
 * @property-read \App\Mark $mark
 * @property-read \App\Renter $renter
 * @property-read \App\Vendor $vendor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereLon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereMarkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereRenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereTotalTimeInUse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereVendorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Car whereVin($value)
 * @mixin \Eloquent
 */
class Car extends Model
{
    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }

    public function mark()
    {
        return$this->belongsTo('App\Mark');
    }

    public function renter()
    {
        return $this->hasOne('App\Renter');
    }

    public function logs()
    {
        return $this->hasMany('App\CarLog');
    }
}
